import React from 'react';
import { connect } from 'react-redux'; 
import { getReviewsApi } from '../apis/hotelsApis';
import Review from './Review';

class Hotel extends React.Component {

  constructor(props) {
    super(props)
  }

  // Show reviews for selected hotel
  showReviews = (id) => {
    const token = localStorage.getItem('token');
    this.props.dispatch(getReviewsApi(token, id));
  }

  // Show hotel details on click
  showHotel = (id) => {
    
  }

  render () {
    const {reviews} = this.props;
    const currentDate = new Date();
    const year = currentDate.getFullYear();
    const month = currentDate.getMonth();
    const day = currentDate.getDay();
    const hours = currentDate.getHours();
    const minutes = currentDate.getMinutes();
    const wholeDate = `${day}/${month}/${year} ${hours}:${minutes}`;

    // let stars = [];
    // for (let i=0; i < this.props.stars.length; i++) {
    //     stars.push(<span className="glyphicon glyphicon-star"></span>);
    // }

    // for (let i=0; i < 5 - this.props.stars.length; i++) {
    //     stars.push(<span className="glyphicon glyphicon-star-empty"></span>);
    // }

    return (
      <div className="row">
        <div className="col-md-4">
          <div className="thumbnail">
            <img src={this.props.image} alt={this.props.name}/>
          </div>
        </div>
        <div className="col-md-8">
          <div className="row margin-bottom20">
            <div className="col-md-4">
              <a href="javascript:void(0);" onClick={this.showHotel.bind(null, this.props.id)}><span>{this.props.name}</span></a> <br />
              <span>{this.props.city} - {this.props.country}</span>
            </div>
            <div className="col-md-4">
              {/*heart here  */}
            </div>
            <div className="col-md-4">
              {/*stars here  */}
            </div>
          </div>
          <div className="row">
            <div className="col-md-12">
              <p>{this.props.description}</p>
            </div>
          </div>
          <div className="row">
            <div className="col-md-2 pull-right">
              <p className="price">{this.props.price}<span className="glyphicon glyphicon-euro"></span></p>
            </div>
          </div>
          <div className="row">
            <div className="col-md-3">
              <p>({wholeDate})</p>
            </div>
            <div className="col-md-3 pull-right">
              <button className="btn btn-primary" type="submit" onClick={this.showReviews.bind(null, this.props.id)}>Show Reviews</button>
            </div>
          </div>
          <hr />
        </div>
        {/*fix same reviews for all hotels  */}
        {reviews.map((review, index) =>
            <Review key={index} {...review} />
        )}
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    reviews: state.reviews
  };
}

export default connect(mapStateToProps)(Hotel);