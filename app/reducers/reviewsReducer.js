import initialState from './initialState';

export default function(state = initialState.reviews, action) {
    switch (action.type) {
      case "GET_HOTEL_REVIEWS":
        return action.reviews;
        break;
      case "GET_HOTEL_REVIEWS_ERROR":
        return action.error;
        break;
    }

  return state;
}