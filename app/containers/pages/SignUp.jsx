import React from 'react';
import { NavLink } from 'react-router-dom';
import { connect } from 'react-redux';
import { signUpApi } from '../../apis/authApis';
import AlertContainer from 'react-alert';

class SignUp extends React.Component {

  constructor() {
    super();
    this.alertOptions = {
      offset: 15,
      position: 'top right',
      theme: 'dark',
      time: 0,
      transition: 'scale'
    }
  }

  signUp = () => {
    const username = document.getElementById('inputUsername').value;
    const password = document.getElementById('inputPassword').value;
    const confirm = document.getElementById('inputPasswordConfirm').value;

    this.msg.removeAll();
    if (username && password && confirm) {
      if (password === confirm) {
        this.props.dispatch(signUpApi(username, password));
      } else {
        this.msg.error('Passwords are not same.');
      }
    } else {
      this.msg.error('All fields are required.');
    }
  }

  render() {
    return (
      <div className="row">
        <div className="col-md-4"></div>
        <div className="col-md-4">
          <h1>Sign Up</h1>
          <div className="form-group">
            <label htmlFor="inputUsername">Username</label>
            <input type="text" id="inputUsername" className="form-control" placeholder="Username" required autoFocus />
          </div>
          <div className="form-group">
            <label htmlFor="inputPassword">Password</label>
            <input type="password" id="inputPassword" className="form-control" placeholder="Password" required />
          </div>
          <div className="form-group">
            <label htmlFor="inputPasswordConfirm">Confirm</label>
            <input type="password" id="inputPasswordConfirm" className="form-control" placeholder="Password" required />
          </div>
          <div className="form-group pull-right">
            <NavLink to='/' replace>Sign In</NavLink>
          </div>
          <button className="btn btn-lg btn-primary btn-block" type="submit" onClick={this.signUp}>Sign Up</button>
        </div>
        <div className="col-md-4"></div>
        <AlertContainer ref={a => this.msg = a} {...this.alertOptions} />
      </div>
    )
  }
};

function mapStateToProps(state) {
  return { auth: state.auth };
}

export default connect(mapStateToProps)(SignUp);