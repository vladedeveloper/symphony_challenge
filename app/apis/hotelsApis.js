import axios from 'axios';
import * as action from '../actions/hotelsActions';

// Get all hotels
export function getHotelsApi(token) {

  return async (dispatch) => {
    try {
      const res = await axios.get(`http://54.77.217.126:8080/hotel_api/`, {headers: {
          "Authorization" : `Token ${token}`
        }
      });

      dispatch(action.getHotels(res.data));
    } catch(error) {
      dispatch(action.getHotelsError(error));
    }
  };
}

// Get reviews for selected hotel
export function getReviewsApi(token, id) {

  return async (dispatch) => {
    try {
      const res = await axios.get(`http://54.77.217.126:8080/hotel_api/get_hotel_reviews/${id}`, {headers: {
          "Authorization" : `Token ${token}`
        }
      });
      if (res.data) {
        dispatch(action.getHotelReviews(res.data));
      } else {
        dispatch(action.getHotelsReviewError('error'));
      }
    } catch(error) {
      dispatch(action.getHotelsReviewsError(error));
    }
  };
}

// Get favorites hotels
export function getHotelsFavoritesApi(token) {

  return async (dispatch) => {
    try {
      const res = await axios.get(`http://54.77.217.126:8080/favorites/`, {headers: {
          "Authorization" : `Token ${token}`
        }
      });

      dispatch(action.getHotelsFavorites(res.data));
    } catch(error) {
      dispatch(action.getHotelsFavoritesError(error));
    }
  };
}