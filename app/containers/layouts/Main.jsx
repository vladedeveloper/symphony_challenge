import React from "react";
import { Switch, Route } from "react-router-dom";
import SignIn from '../pages/SignIn';
import SignUp from '../pages/SignUp';
import Dashboard from '../pages/Dashboard';
import NotFound from '../pages/NotFound';
import Favorites from '../pages/Favorites'

import requireAuth from '../../utils/RequireAuth';
import noRequireAuth from '../../utils/noRequireAuth';

export default class Main extends React.Component {

  render() {
    
    return (
      <div className="container">
        <Switch>
          <Route exact path="/" component={requireAuth(Dashboard)}/>
          <Route exact path="/signin" component={noRequireAuth(SignIn)}/>
          <Route exact path="/signup" component={noRequireAuth(SignUp)}/>
          <Route exact path="/favorites" component={requireAuth(Favorites)}/>
          <Route render={() => <NotFound />} />
        </Switch>
      </div>
    );
  }
}