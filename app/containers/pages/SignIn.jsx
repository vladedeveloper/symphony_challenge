import React from 'react';
import { NavLink } from 'react-router-dom';
import { connect } from 'react-redux';
import { signInApi } from '../../apis/authApis';
import AlertContainer from 'react-alert';

class SignIn extends React.Component {

  constructor() {
    super();
    // Alert options
    this.alertOptions = {
      offset: 15,
      position: 'top right',
      theme: 'dark',
      time: 0,
      transition: 'scale'
    }
  }

  signIn = () => {
    const username = document.getElementById('inputUsername').value;
    const password = document.getElementById('inputPassword').value;
    this.msg.removeAll();
    if (username && password) {
      this.props.dispatch(signInApi(username, password)).then(() => {
        if (this.props.auth === 'error') {
          this.msg.error('Username or password is not correct.');
        } else {
          localStorage.setItem('user', true);
          localStorage.setItem('token', this.props.token);
        }
      });
    } else {
      this.msg.error('All fields are required.');
    }
  }

  render() {
    return (
      <div className="row">
        <div className="col-md-4"></div>
        <div className="col-md-4">
          <h1>Sign In</h1>
          <div className="form-group">
            <label htmlFor="inputUsername">Username</label>
            <input type="text" id="inputUsername" className="form-control" placeholder="Username" required autoFocus />
          </div>
          <div className="form-group">
            <label htmlFor="inputPassword">Password</label>
            <input type="password" id="inputPassword" className="form-control" placeholder="Password" required />
          </div>
          <div className="form-group">
            <NavLink to='/signup' replace>Sign Up</NavLink>
          </div>
          <button className="btn btn-lg btn-primary btn-block" type="submit" onClick={this.signIn}>Sign in</button>
        </div>
        <div className="col-md-4"></div>
        <AlertContainer ref={a => this.msg = a} {...this.alertOptions} />
      </div>
    )
  }
};

function mapStateToProps(state) {
  return { 
          auth: state.autentication.auth,
          token: state.autentication.token
        };
}

export default connect(mapStateToProps)(SignIn);