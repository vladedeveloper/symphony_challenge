import React from 'react';
import { connect } from 'react-redux';
import Navbar from '../layouts/Navbar';

class NotFound extends React.Component {

  constructor() {
    super();

  }

  render() {
    return (
      <div>
        <Navbar />
        Page not found
      </div>
    )
  }
};

function mapStateToProps(state) {
  return { auth: state.auth };
}

export default connect(mapStateToProps)(NotFound);