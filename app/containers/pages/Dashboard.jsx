import React from 'react';
import { connect } from 'react-redux';
import { getHotelsApi } from '../../apis/hotelsApis';
import Navbar from '../layouts/Navbar';
import Hotel from '../../components/Hotel';

class Dashboard extends React.Component {

  constructor() {
    super();
  }

  loadHotels = () => {
    let token = localStorage.getItem('token');
    this.props.dispatch(getHotelsApi(token));
  }

  render() {
    let {hotels} = this.props;
    return (
      <div>
        <Navbar />
        <div>
          <button onClick={this.loadHotels}>Load Hotels</button>
          
          {
            hotels != 'error' ?
                hotels.map((hotel, index) =>
                  <Hotel key={index} {...hotel} />
                )
            :
              <div>Error</div>
          }
        </div>
      </div>
    )
  }
};

function mapStateToProps(state) {
  return {
    hotels: state.hotels
  };
}

export default connect(mapStateToProps)(Dashboard);