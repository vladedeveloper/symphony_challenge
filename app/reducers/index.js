import {combineReducers} from 'redux';
import autentication from './authReducer';
import hotels from './hotelsReducer';
import reviews from './reviewsReducer';
import favorites from './favoritesReducer';

const allReducers = combineReducers({
  autentication,
  hotels,
  reviews,
  favorites
});

export default allReducers;