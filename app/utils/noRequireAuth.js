import React, { Component } from 'react';
import { connect } from 'react-redux';

export default function (ComposedComponent) {
  class NoAuthentication extends Component {
    componentWillMount() {
      if (this.props.auth === true) {
        this.props.history.push('/');
      }
    }

    componentWillUpdate(nextProps) {
      if (nextProps.auth === true) {
        this.props.history.push('/');
      }
    }

    render() {
      return <ComposedComponent />;
    }
  }

  function mapStateToProps(state) {
    return { auth: state.autentication.auth };
  }

  return connect(mapStateToProps)(NoAuthentication);
}