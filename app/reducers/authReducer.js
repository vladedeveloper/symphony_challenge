import initialState from './initialState';

export default function(state = initialState.auth, action) {
    switch (action.type) {
      case "CHANGE_AUTH":
        return {...state, auth: action.auth, token: action.token};
        break;
      case "AUTH_ERROR":
        return action.error;
        break;
    }

  return state;
}