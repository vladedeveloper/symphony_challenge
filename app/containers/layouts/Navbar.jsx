import React from "react";
import { NavLink } from 'react-router-dom';
import { connect } from 'react-redux';
import { authenticate } from '../../actions/authActions';

class Navbar extends React.Component {

  logout = () => {
    this.props.dispatch(authenticate(false));
    localStorage.clear();
  }

  render() {
    return (
      <header className="main-header">
        <nav className="navbar navbar-inverse">
          <div className="container-fluid">
            <ul className="nav navbar-nav">
              <li><NavLink to="/">Dashboard</NavLink></li>
              <li><NavLink to="/favorites">Favorites</NavLink></li>
            </ul>
            <ul className="nav navbar-nav navbar-right">
              <li><a href="#"><span className="glyphicon glyphicon-user"></span></a></li>
              <li><a href="javascript:void(0);" onClick={this.logout}><span className="glyphicon glyphicon-log-out"></span> Sign Out</a></li>
            </ul>
          </div>
        </nav>
      </header>
    )
  }
};

function mapStateToProps(state) {
  return { auth: state.auth };
}

export default connect(mapStateToProps)(Navbar);