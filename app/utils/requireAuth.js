import React, { Component } from 'react';
import { connect } from 'react-redux';

export default function (ComposedComponent) {
  class Authentication extends Component {
    componentWillMount() {
      if (this.props.auth !== true) {
        this.props.history.push('/signin');
      }
    }

    componentWillUpdate(nextProps) {
      if (nextProps.auth !== true) {
        this.props.history.push('/signin');
      }
    }

    render() {
      return <ComposedComponent />;
    }
  }

  function mapStateToProps(state) {
    return { auth: state.autentication.auth };
  }

  return connect(mapStateToProps)(Authentication);
}