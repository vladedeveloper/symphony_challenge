import React from 'react';
import { connect } from 'react-redux';
import { getHotelsFavoritesApi } from '../../apis/hotelsApis';
import Navbar from '../layouts/Navbar';
import Hotel from '../../components/Hotel';

class Favorites extends React.Component {

  constructor() {
    super();
  }

  componentWillMount() {
    let token = localStorage.getItem('token');
    this.props.dispatch(getHotelsFavoritesApi(token));
  }

  render() {
    let {favorites} = this.props;
    return (
      <div>
        <Navbar />
        {favorites.map((favorite, index) =>
          <Hotel key={index} {...favorite} />
        )}
      </div>
    )
  }
};

function mapStateToProps(state) {
  return {
    favorites: state.favorites
  };
}

export default connect(mapStateToProps)(Favorites);