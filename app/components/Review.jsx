import React from 'react';

const Review = (props) => {

    return (
      <div className="row">
        <div className="row">
        <div className="col-md-4">
              <p>{props.author.first_name} {props.author.last_name}</p>
          </div>
        </div>
        <div className="row">
        <div className="col-md-4">
              <p>{props.message}</p>
          </div>
        </div>
      </div>
    )
}

export default Review;