export function authenticate(auth, token) {
  return { type: "CHANGE_AUTH", auth, token };
}

export function authError(error) {
  return { type: "AUTH_ERROR", error: 'error' };
}