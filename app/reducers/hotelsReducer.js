import initialState from './initialState';

export default function(state = initialState.hotels, action) {
    switch (action.type) {
      case "GET_ALL_HOTELS":
        return action.hotels;
        break;
      case "GET_ALL_HOTELS_ERROR":
        return action.error;
        break;
    }

  return state;
}