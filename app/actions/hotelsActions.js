export function getHotels(hotels) {
  return { type: "GET_ALL_HOTELS", hotels: hotels };
}

export function getHotelsError(error) {
  return { type: "GET_ALL_HOTELS_ERROR", error: 'error' };
}

export function getHotelReviews(reviews) {
  return { type: "GET_HOTEL_REVIEWS", reviews: reviews };
}

export function getHotelsReviewsError(error) {
  return { type: "GET_HOTEL_REVIEWS_ERROR", error: 'error' };
}

export function getHotelsFavorites(favorites) {
  return { type: "GET_HOTEL_FAVORITES", favorites: favorites };
}

export function getHotelsFavoritesError(error) {
  return { type: "GET_HOTEL_FAVORITES_ERROR", error: 'error' };
}