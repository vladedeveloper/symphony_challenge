import initialState from './initialState';

export default function(state = initialState.favorites, action) {
    switch (action.type) {
      case "GET_HOTEL_FAVORITES":
        return action.favorites;
        break;
      case "GET_HOTEL_FAVORITES_ERROR":
        return action.error;
        break;
    }

  return state;
}