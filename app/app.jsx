import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { HashRouter } from "react-router-dom";

import configureStore from './configureStore';
import { authenticate } from './actions/authActions';

import "../public/css/style.scss"; 

import Layout from "./containers/layouts/Layout";

const store = configureStore();

// If user logged autenticate him on reload page
const user = localStorage.getItem('user');
const token = localStorage.getItem('token');
if(user && token) {
  store.dispatch(authenticate(true, token));
}

const app = document.getElementById("app");

ReactDOM.render(
  <Provider store={store}>
    <HashRouter>
      <Layout />
    </HashRouter>
  </Provider>,
  app
);