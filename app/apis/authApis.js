import axios from 'axios';
import { authenticate, authError } from '../actions/authActions';

// Api for registration
export function signUpApi(username, password) {
  return async (dispatch) => {
    try {
      const res = await axios.post(`http://54.77.217.126:8080/register/`, { username, password });
      // no token for registrated user
      //dispatch(authenticate(true, token));
    } catch(error) {
      dispatch(authError(error));
    }
  };
}

// Api for login
export function signInApi(username, password) {
  return async (dispatch) => {
    try {
      const res = await axios.post(`http://54.77.217.126:8080/api-token-auth/`, { username, password });
      if (res.data && res.data.token) {
        dispatch(authenticate(true, res.data.token));
      } else {
        dispatch(authError('error'));
      }
    } catch(error) {
      dispatch(authError(error));
    }
  };
}